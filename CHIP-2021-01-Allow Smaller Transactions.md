# CHIP 2021-01 Allow Transactions to be smaller in size

## Summary

This proposal updates the rule for minimum transation sizes in order to simplify the burden on library software and on mining software, while still protecting users of SPV nodes from being harmed by an attacker.  It reduces the current consensus minimum transaction size of 100 bytes down to a minimum size of 65 bytes.  This more relaxed 65-byte minimum size, like the previous 100-byte minimum size, applies to both coinbase and non-coinbase transactions uniformly.

> Title: Allow Transactions to be smaller in size  
> Date: 2021-01  
> Type: Consensus  
> Status: Draft  
> Version: 0.4  
> Last edit: 2022-8-14  
> Owner: Tom Zander  
> History: https://gitlab.com/bitcoin.cash/chips  

## Motivation and Benefits

In June 2018 research was published surrounding a weakness in the interaction of Merkle-tree and Bitcoin's Merkle-block message, as mostly used by SPV wallets.

From [Leaf-Node weakness in Bitcoin Merkle Tree Design](https://bitslog.wordpress.com/2018/06/09/leaf-node-weakness-in-bitcoin-merkle-tree-design/):

> To summarize, the most space-efficient solution without a soft-fork would be hybrid: show inclusion proof of the rightmost TX id branch if the tree is not full, else **the coinbase** (showing mid-state and length) **if length is greater than 64 bytes**, or else show the full coinbase transaction.

The problem is that transactions of size exactly 64 bytes become a small but certain weakness that have potential of being exploited in order to defraud or steal from honest players.

In the November 2018 Protocol upgrade Bitcoin Cash reacted to this by making the transaction size have a minimum of 100 bytes. As a result this exploit is no longer possible on Bitcoin Cash.

This solution, however, created several problems.

The first and most often encountered problem is that a default coinbase transaction is smaller than 100 bytes (in many common cases it is 98 bytes or smaller). This means that new mining software creating a coinbase will often not satisfy the 100-byte minimum (which is an expensive mistake to make).

Additionally, it is possible to create non-standard transactions which are smaller than 100 bytes, and which are completely valid transactions (although they are not accepted for relaying on main net).

These usecases all show that the entire ecosystem needs to take this rather large minimum into account, a minimum which is unique to Bitcoin Cash (Bitcoin Core does have an 82-byte minimum but it is for relaying-only and does not apply to mined transaction), and a minimum that has been rather arbitrarily chosen and, in hindsight, has proven to be too large.

Because the minimum is rather large, developers of libraries and software will inadvertently violate this minimum in many cases as well in their libraries and testing software where the transactions created for testing may not be about real money, and thus the scripts will not reflect reality (such as trivial non-standard `OP_TRUE` locking scripts). There are many places where automated testing systems had to be adjusted to account for this 100-byte minimum.  Reducing the minimum to 65 bytes is far less likely to trigger errors in such systems

## An Alternative Approach

An alternative approach would have been to not have a *minimum* size at all, per se, but rather instead to simply forbid transactions that are *precisely* 64-bytes in size.  This would eliminate the leaf node merkle attack, while still offering testing libraries the option to create transactions sized 60, 61, 62 or 63 bytes (the smallest possible transaction one can ever create is 60 bytes).  However, during discussion of this CHIP it was argued that since we already have a minimum size, it is easier on existing software to simply relax the 100-byte minimum (which is often implemented simply as a programming constant) down to 65 bytes (thus requiring a simple programming constant change, rather than the slightly increased programming complexity of requiring a minimum pre-activation of the upgrade, while forbidding exactly one specific value post-activation).

Moreover, it was opined by some node engineers that it is more likely for software that creates transactions to trigger an error if consensus imposes a minimum size, in which case authors of libraries would be more likely to be made "aware" of Bitcoin Cash's own peculiar rules regarding transaction sizes.  If we allowed sizes of 60, 61, 62, & 63, then it might be more likely that software authors that develop for BCH and that maintain software that can create very small transactions would never even notice that just the 64-byte size is forbidden, whereas if a range of sizes is disallowed (60-64), the likelihood would be increased for a developer to notice that BCH has peculiar rules for small transaction sizes.


## Impact / Costs and Risks

The impact on the ecosystem is split into full nodes and others.

* Full nodes must simply relax the consensus test from 100 bytes down to 65 bytes.  This is a widening of the allowed transaction consensus rules and therefore should be scheduled for a future protocol upgrade.  Nodes do need to ensure that the test uses the right minimum sizes both pre-activation and post-activation of the upgrade.

* All the players that are not full nodes do not have to change anything, as anything they generated before that safisfied the old 100-byte minimum is still allowed under the more relaxed 65-byte minimum.

This CHIP is mostly aimed at cleaning up technical debt. The result is a more relaxed minimum for most software, making it less likely that normally operating sofware will violate the new more relaxed minimum. Additionally, should the 64-byte transaction size exploit get solved in the future on the p2p layer, then we can simply drop any rules enforcing a minimum transaction size altogether.

## Proposal Description

To solve the unintended side-effects of the rather aggressive 100-byte minimum, we simply relax this minimum to 65 bytes. This minimum size applies to both coinbase and non-coinbase transactions uniformly.

## References:

Discussion on [bitcoincashresearch](https://bitcoincashresearch.org/t/154)

[Leaf-Node weakness in Bitcoin Merkle Tree Design](https://bitslog.wordpress.com/2018/06/09/leaf-node-weakness-in-bitcoin-merkle-tree-design/)

[merge request for Implementation in BCHN](https://gitlab.com/bitcoin-cash-node/bitcoin-cash-node/-/merge_requests/1598)

## Statements

**Freetrader, BCHN release manager.**  
> I reviewed this and would support activation of this CHIP (revision [75b97e22a3dc295de7373255025f38cd0911b866](https://gitlab.com/bitcoin.cash/chips/-/blob/75b97e22a3dc295de7373255025f38cd0911b866/CHIP-2021-01-Allow%20Smaller%20Transactions.md)) in the first network upgrade after May 2021. ([source](https://bitcoincashresearch.org/t/154/32))

**Benjamin Scherrey ([proteusguy](https://bitcoincashresearch.org/u/proteusguy)), ActorForth** 
> I’ve reviewed the CHIP and support it as written. My interest is as a development stakeholder. ([source](https://bitcoincashresearch.org/t/154/31))

**Andrew Stone, developer Bitcoin Unlimited**  
> Awemany (working as a BU dev) and I originally brought this problem to the BCH community’s attention. When ABC chose to limit at 100 bytes I pointed out this was idiocy way back before the original fork, and was ignored. So BU is happy to support a change that fixes yet-another-dumb-decision autocratically made during the ABC days. ([source](https://bitcoincashresearch.org/t/154/36))

**Josh Green, developer Bitcoin Verde**  
After merging PR #5, fixing the concept to be "larger than 65 bytes"  

> We've held the opinion that either is fine.  So if BCHN is taking that path then we're happy to align with that.  ([source](https://t.me/bitcoinverde/3542))


## Copyright Notice

Copyright (C)  2021 Tom Zander  
Copyright (C)  2022 Calin Culianu

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.

